var dataServidor = null;
function peticionAjaxServidor(d, type, dataType, url, cb) {
    $.ajax({ url: url, type: type, cache: !1, dataType: dataType, data: { data: JSON.stringify(d) } })
        .done(function (d) {
            cb(d);
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            cb({ code: -1, data: "Lo sentimos, por favor recarga la página" });
        });
}
function mostrarNotificacion(show) {
    show ? $("#notificacion").addClass("show") : $("#notificacion").removeClass("show");
}
function cambiarTextoNotificacion(sms) {
    $("#noti_texto").text(sms);
}
function mostrarLoader(show) {
    show ? $("#loader").removeClass("hide") : $("#loader").addClass("hide");
}
function mostrarCerrar(show) {
    show ? $("#cerrar_notificacion").removeClass("hide") : $("#cerrar_notificacion").addClass("hide");
}
function geAnuncios(anuncios, clientes, equipo) {
    let ads = { 0: [], 1: [], 2: [], 3: [], 4: [] },
        contador = 0;
    for (; contador < 5; ) {
        let aux = [];
        anuncios.forEach((ad) => {
            ad.data.prioridad === contador &&
                clientes.forEach((c) => {
                    c.id === ad.data.id_cliente &&
                        0 === c.data.cuenta.estado &&
                        ad.data.clicks.contador < ad.data.clicks.pagados + ad.data.clicks.gratis &&
                        (ad.data.equipos.indexOf("all") >= 0
                            ? ad.data.id_provincia.indexOf(equipo.data.id_provincia) >= 0 && ad.data.id_canton.indexOf(equipo.data.id_canton) >= 0 && aux.push(ad)
                            : ad.data.equipos.indexOf(equipo.id) && aux.push(ad));
                });
        }),
            (ads[contador] = aux),
            (contador += 1);
    }
    return ads;
}
function getAdToShow(ads) {
    let ad = null,
        contador = 0;
    for (; contador < 5 && null === ad; ) {
        let g = ads[contador];
        if (g.length > 0) {
            let r;
            ad = g[Math.round(Math.random() * (g.length - 1))];
        }
        contador += 1;
    }
    return ad;
}
function getSubCliente(ad, subclientes) {
    let data_cliente;
    return (
        subclientes.forEach((sub) => {
            ad.data.id_subcliente === sub.id && (data_cliente = sub);
        }),
        data_cliente
    );
}
function cargarAnuncio({ anuncios: anuncios, clientes: clientes, equipo: equipo, subclientes: subclientes }) {
    let ads,
        ad = getAdToShow(geAnuncios(anuncios, clientes, equipo));
    console.log(ad), $("#video").addClass("hide"), $("#imagen").addClass("hide");
    let data_cliente = getSubCliente(ad, subclientes);
    if (
        ($(".titulo_anuncio").text(data_cliente.data.etiqueta),
        $(".icono_anuncio").text(data_cliente.data.etiqueta.split("")[0]),
        $("#contador").attr("data-anuncio", ad.id),
        $("#contador").attr("data-equipo", equipo.id),
        $("#contador").attr("data-cliente", ad.data.id_cliente),
        $("#contador").attr("data-subcliente", ad.data.id_subcliente),
        "0/imagen" === ad.data.tipo)
    ) {
        $("#ad_descripcion_imagen_text").text(ad.data.descripcion), $("#src_imagen").attr("src", ad.data.url);
        let img = document.getElementById("src_imagen"),
            loaded = null;
        loaded = setInterval(
            function () {
                if (img.complete && 0 !== img.naturalHeight) {
                    $("#imagen").removeClass("hide"), mostrarLoader(!1), mostrarNotificacion(!1);
                    let segundos = 20,
                        interval = null;
                    (interval = setInterval(
                        () => {
                            segundos >= 0 ? ($("#contador").text(segundos), segundos--) : (null != interval && clearInterval(interval), $("#contador").text("x"), $("#contador").removeClass("noactive"));
                        },
                        1e3,
                        segundos,
                        interval
                    )),
                        null != loaded && clearInterval(loaded);
                }
            },
            500,
            img,
            loaded
        );
    } else if ("1/video" === ad.data.tipo) {
        $("#ad_descripcion_video_text").text(ad.data.descripcion);
        let $source = $("#src_video");
        ($source[0].src = ad.data.url), $source.parent()[0].load(), $("#video").removeClass("hide");
        let video = document.getElementById("tag_video"),
            segundos = 20;
        cambiarTextoNotificacion("Por favor dale play para continuar"),
            mostrarLoader(!1),
            mostrarCerrar(!0),
            setTimeout(() => {
                mostrarNotificacion(!1), mostrarCerrar(!1);
            }, 1e4),
            video.addEventListener("playing", function () {
                $("#notificacion").hasClass("show") && mostrarNotificacion(!1);
                let interval = null;
                interval = setInterval(
                    () => {
                        segundos >= 0 ? ($("#contador").text(segundos), (segundos -= 1)) : (null != interval && clearInterval(interval), $("#contador").text("x"), $("#contador").removeClass("noactive"));
                    },
                    1e3,
                    segundos,
                    interval
                );
            });
    }
}
function ponerFondo(id, url, size, repeat, position) {
    $("#" + id).css("background-image", "url(" + url + ")"), $("#" + id).css("background-size", size), $("#" + id).css("background-repeat", repeat), $("#" + id).css("background-position", position);
}
function ponerTexto(id, texto) {
    $("#btn_" + id + "_texto").text(texto);
}
function ponerDataModulo(id, dm, dt, dc, tit) {
    $("#" + id).attr("data-maqueta", dm), $("#" + id).attr("data-tipo", dt), $("#" + id).attr("data-cliente", dc), $("#" + id).attr("data-titulo", tit);
}
function cargarMaqueta({ maqueta: maqueta }) {
    let ancho = $(window).width(),
        files = null,
        iconos = maqueta.data.iconos;
    ancho >= 320 && ancho <= 767
        ? window.matchMedia("(orientation: portrait)").matches && (files = maqueta.data.artes_movil)
        : ancho >= 768 && ancho <= 1365
        ? window.matchMedia("(orientation: portrait)").matches && (files = maqueta.data.artes_tablet)
        : ancho >= 1025 && (files = maqueta.data.artes_pc),
        null !== files &&
            (ponerFondo("banner", files.banner, "100%", "no-repeat", "center"),
            ponerFondo("img_fondo", files.fondo, "auto", "no-repeat", "center"),
            ponerFondo("btn_a_icono", iconos.a.icono, "100% 100%", "no-repeat", "center"),
            ponerFondo("btn_b_icono", iconos.b.icono, "100% 100%", "no-repeat", "center"),
            ponerFondo("btn_c_icono", iconos.c.icono, "100% 100%", "no-repeat", "center"),
            ponerFondo("btn_d_icono", iconos.d.icono, "100% 100%", "no-repeat", "center"),
            ponerTexto("a", iconos.a.etiqueta),
            ponerTexto("b", iconos.b.etiqueta),
            ponerTexto("c", iconos.c.etiqueta),
            ponerTexto("d", iconos.d.etiqueta),
            ponerDataModulo("btn_a_icono", maqueta.id, iconos.a.tipo, maqueta.data.id_cliente, iconos.a.titulo),
            ponerDataModulo("btn_b_icono", maqueta.id, iconos.b.tipo, maqueta.data.id_cliente, iconos.b.titulo),
            ponerDataModulo("btn_c_icono", maqueta.id, iconos.c.tipo, maqueta.data.id_cliente, iconos.c.titulo),
            ponerDataModulo("btn_d_icono", maqueta.id, iconos.d.tipo, maqueta.data.id_cliente, iconos.d.titulo));
}
(window.onresize = function (event) {
    null !== this.dataServidor && cargarMaqueta(this.dataServidor.d);
}),
    $("#contador").click(function () {
        $("#contador").addClass("noactive");
        let date = new Date(),
            d = {
                id_anuncio: $(this).attr("data-anuncio"),
                id_equipo: $(this).attr("data-equipo"),
                id_cliente: $(this).attr("data-cliente"),
                id_subcliente: $(this).attr("data-subcliente"),
                dia: date.getDate(),
                mes: date.getMonth() + 1,
                ano: date.getFullYear(),
                time: { hora: date.getHours(), minutos: date.getMinutes(), segundos: date.getSeconds() },
            },
            saldo_suficiente = !1;
        null !== dataServidor &&
            dataServidor.d.clientes.forEach((c) => {
                c.id === d.id_cliente && c.data.cuenta.saldo > 0.02 && (saldo_suficiente = !0);
            }),
            cambiarTextoNotificacion("Espere por favor"),
            mostrarLoader(!0),
            mostrarNotificacion(!0),
            saldo_suficiente
                ? peticionAjaxServidor(d, "POST", "json", url_server + "/newspoth/addClick", (data) => {
                      0 === data.code ? (mostrarLoader(!1), mostrarNotificacion(!1), $("#anuncio").css("display", "none")) : (mostrarLoader(!1), cambiarTextoNotificacion("Error, recarga la página"));
                  })
                : (cambiarTextoNotificacion("El saldo de tu cuenta se ha  agotado"), mostrarLoader(!1));
    });
let contador = 0,
    contador_interval = null;
function getMobileOperatingSystem() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    return /windows phone/i.test(userAgent) ? { nu: 0, so: "Windows Phone" } : /android/i.test(userAgent) ? { nu: 1, so: "Android" } : /iPad|iPhone|iPod/.test(userAgent) && !window.MSStream ? { nu: 2, so: "iOS" } : "unknown";
}
function start(codigo, mac, url_server) {
    cambiarTextoNotificacion("Cargando contenido..."),
        mostrarLoader(!0),
        mostrarNotificacion(!0),
        codigo.length > 0
            ? peticionAjaxServidor({ codigo: codigo, so: getMobileOperatingSystem() }, "POST", "json", url_server + "/newspoth/getDataAppGeneral", (data) => {
                  console.log(data),
                      0 === data.code
                          ? ((dataServidor = data),
                            data.d.anuncios.length > 0
                                ? 0 == login_fb_status
                                    ? (cargarAnuncio(data.d), cargarMaqueta(data.d))
                                    : (cargarMaqueta(data.d),
                                      mostrarLoader(!1),
                                      mostrarNotificacion(!1),
                                      $("#anuncio").addClass("hide"),
                                      (contador_interval = setInterval(
                                          () => {
                                              16 === contador ? ($("#likefb").addClass("hide"), $("#login_mac").removeClass("hide"), $("#login_trial").removeClass("hide"), clearInterval(contador_interval)) : (contador += 1);
                                          },
                                          1e3,
                                          contador,
                                          contador_interval
                                      )))
                                : (cambiarTextoNotificacion("No se registran anuncios para mostrar, intenta luego"), mostrarLoader(!1)))
                          : (cambiarTextoNotificacion("Error, por favor recarga la página"), mostrarLoader(!1));
              })
            : (cambiarTextoNotificacion("Something has gone wrong"), mostrarLoader(!1));
}
function getCampoDeArray(d, campo) {
    let aux = [];
    return (
        d.forEach((i) => {
            aux.indexOf(i[campo]) < 0 && aux.push(i[campo]);
        }),
        aux
    );
}
$("#cerrar_div_modulo").click(function () {
    $("#div_modulo").removeClass("show"), $(".mod_lista").addClass("hide"), $("#mod_titulo_span").text("");
});
var contenido_modulos = [];
function cargarContenidoTurismo(modulo) {
    let { contenido: contenido } = modulo.data,
        categorias = getCampoDeArray(contenido.b, "categoria");
    $("#lista_cat").empty(),
        $("#lista_cat").append('<div class="item active btn_action_item" data-target="todos" data-clase="lugar"><span>Todos</span></div>'),
        categorias.forEach((cat) => {
            let html = '<div class="item btn_action_item" id="btn_cat_' + cat + '" data-clase="lugar" data-target="lugar_cat_' + cat.split("/")[0] + '" data-modulo="' + modulo.id + '"><span>' + cat.split("/")[1] + "</span></div>";
            $("#lista_cat").append(html);
        }),
        $("#lista_lugares").empty(),
        contenido.b.forEach((c) => {
            let html = '<div class="lugar todos lugar_cat_' + c.categoria.split("/")[0] + ' show">';
            (html += '   <img src="' + c.url + '">'),
                (html += '   <div class="color"></div>'),
                (html += '   <div class="barra">'),
                (html += '       <div class="logo">'),
                (html += '           <span class="icono">LT</span>'),
                (html += "       </div>"),
                (html += '       <div class="nombre">'),
                (html += '           <span class="texto">' + c.etiqueta + "</span>"),
                (html += "       </div>"),
                (html += "   </div>"),
                (html += '   <div class="descripcion">'),
                (html += "       <span>" + c.descripcion + "</span>"),
                (html += "   </div>"),
                (html += "</div>"),
                $("#lista_lugares").append(html);
        }),
        $("#mod_2_turismo").removeClass("hide");
}
function cargarContenidoPromociones(modulo) {
    let { contenido: contenido } = modulo.data;
    $("#lista_promociones").empty(),
        contenido.b.forEach((c) => {
            let html = '<div class="promo show">';
            (html += '   <div class="barra">'),
                (html += '       <div class="logo">'),
                (html += '           <span class="icono">P</span>'),
                (html += "       </div>"),
                (html += '       <div class="nombre">'),
                (html += '           <span class="texto">' + c.etiqueta + "</span>"),
                (html += "       </div>"),
                (html += "   </div>"),
                (html += '   <div class="descripcion"'),
                (html += '       <span class="texto">' + c.descripcion + "</span>"),
                (html += "   </div>"),
                (html += '   <div class="multimedia">'),
                (html += '       <img class="materialboxed" src="' + c.url + '" class="imagen">'),
                (html += "   </div>"),
                (html += "</div>"),
                $("#lista_promociones").append(html),
                $(".materialboxed").materialbox();
        }),
        $("#mod_1_promociones").removeClass("hide");
}
function getProvinciaCanton(provincias, cantones, id_p, id_c) {
    let pc = { provincia: "", canton: "" };
    return (
        provincias.forEach((p) => {
            p.data.id_provincia === id_p && (pc.provincia = p.data.etiqueta);
        }),
        cantones.forEach((c) => {
            c.id === id_c && (pc.canton = c.data.etiqueta);
        }),
        pc
    );
}
function cargarContenidoPuntosWifi({ equipos: equipos, provincias: provincias, cantones: cantones }) {
    $("#lista_cantones").empty(),
        $("#lista_cantones").append('<div class="item active btn_action_item" data-target="todos" data-clase="punto"><span>Todos</span></div>'),
        cantones.forEach((canton) => {
            let html = '<div class="item btn_action_item" id="btn_canton_' + canton.id + '" data-clase="punto" data-target="punto_canton_' + canton.id + '" data-modulo=""><span>' + canton.data.etiqueta + "</span></div>";
            $("#lista_cantones").append(html);
        }),
        $("#lista_puntos").empty(),
        equipos.forEach((equipo) => {
            let html = '<div class="punto todos punto_canton_' + equipo.data.id_canton + ' show">';
            (html += '   <div class="centro">'),
                (html += '       <div class="logo">'),
                (html += '           <span class="icono">' + equipo.data.lugar.nombre.split("")[0] + "</span>"),
                (html += "       </div>"),
                (html += '       <div class="barra">'),
                (html += "           <span></span>"),
                (html += "       </div>"),
                (html += '       <div class="datos">'),
                (html += '           <span class="nombre">' + equipo.data.lugar.nombre + "</span>"),
                (html += '           <span class="texto">' + equipo.data.lugar.direccion + ", " + equipo.data.lugar.telefono + "</span>");
            let pc = getProvinciaCanton(provincias, cantones, equipo.data.id_provincia, equipo.data.id_canton);
            console.log(pc), (html += '           <span class="texto">' + pc.provincia + ", " + pc.canton + "</span>"), (html += "       </div>"), (html += "   </div>"), (html += "</div>"), $("#lista_puntos").append(html);
        }),
        $("#mod_4_puntos_wifi").removeClass("hide");
}
function cargarContenidoVideos(modulo) {
    let { contenido: contenido } = modulo.data;
    $("#lista_videos").empty(),
        console.log(contenido),
        contenido.b.forEach((item) => {
            let html = '<div class="video" id="bloque_video_' + item.id + '">';
            (html += '   <div class="video_titulo">'),
                (html += '       <i class="material-icons icono">video_library</i>'),
                (html += '       <span class="nombre">' + item.etiqueta + "</span>"),
                (html += "   </div>"),
                (html += '   <div class="video_descripcion">'),
                (html += '       <span class="texto">' + item.descripcion + "</span>"),
                (html += '       <div class="opcion">'),
                (html += "           <span>Ver más</span>"),
                (html += '           <i class="material-icons icono btn_more_video" id="btn_more_video_' + item.id + '" data-id="' + item.id + '">keyboard_arrow_down</i>'),
                (html += "       </div>"),
                (html += "   </div>"),
                (html += '   <div class="video_media">'),
                (html += '       <video id="reproductor_video_' + item.id + '" controls autoplay muted>'),
                (html += '           <source id="source_video_' + item.id + '" src="">'),
                (html += "       </video>"),
                (html += '       <div class="miniatura" id="miniatura_video_' + item.id + '">'),
                (html += '           <i class="material-icons btn_play_video" id="play_video_' + item.id + '" data-id="' + item.id + '" data-url="' + item.url + '">play_arrow</i>'),
                (html += '           <div class="preloader-wrapper small active loader" id="loader_video_' + item.id + '">'),
                (html += '               <div class="spinner-layer spinner-red-only">'),
                (html += '                   <div class="circle-clipper left">'),
                (html += '                       <div class="circle"></div>'),
                (html += '                   </div><div class="gap-patch">'),
                (html += '                       <div class="circle"></div>'),
                (html += '                   </div><div class="circle-clipper right">'),
                (html += '                       <div class="circle"></div>'),
                (html += "                   </div>"),
                (html += "               </div>"),
                (html += "           </div>"),
                (html += "       </div>"),
                (html += "   </div>"),
                (html += "</div>"),
                $("#lista_videos").append(html);
        }),
        $("#mod_0_video").removeClass("hide");
}
$("body").on("click", ".btn_action_item", function () {
    $(".btn_action_item").removeClass("active"), $(this).addClass("active");
    let dt = $(this).attr("data-target"),
        clase = $(this).attr("data-clase");
    console.log(dt), $("." + clase).removeClass("show"), $("." + dt).addClass("show");
}),
    $("body").on("click", ".btn_play_video", function () {
        let id = $(this).attr("data-id"),
            url = $(this).attr("data-url");
        $("#play_video_" + id).addClass("notshow"), $("#loader_video_" + id).addClass("show");
        let $source = $("#source_video_" + id),
            video;
        ($source[0].src = url),
            $source.parent()[0].load(),
            document.getElementById("reproductor_video_" + id).addEventListener("playing", function () {
                $("#loader_video_" + id).removeClass("show"), $("#miniatura_video_" + id).addClass("notshow"), $("#reproductor_video_" + id).addClass("show");
            });
    }),
    $("body").on("click", ".btn_more_video", function () {
        let id = $(this).attr("data-id");
        $("#bloque_video_" + id).hasClass("showtexto")
            ? ($("#btn_more_video_" + id).text("keyboard_arrow_down"), $("#bloque_video_" + id).removeClass("showtexto"))
            : ($("#btn_more_video_" + id).text("keyboard_arrow_up"), $("#bloque_video_" + id).addClass("showtexto"));
    });
var dataCalendario = null;
let fecha_actual = {},
    fecha_ahora = { dia: new Date().getDate(), mes: new Date().getMonth(), ano: new Date().getFullYear() },
    contador_mes = 0,
    before_mes = 0,
    after_mes = 11 - fecha_ahora.mes;
function mesPorNumero(mes) {
    let a = "";
    switch (mes) {
        case 0:
            a = "Enero";
            break;
        case 1:
            a = "Febrero";
            break;
        case 2:
            a = "Marzo";
            break;
        case 3:
            a = "Abril";
            break;
        case 4:
            a = "Mayo";
            break;
        case 5:
            a = "Junio";
            break;
        case 6:
            a = "Julio";
            break;
        case 7:
            a = "Agosto";
            break;
        case 8:
            a = "Septiembre";
            break;
        case 9:
            a = "Octubre";
            break;
        case 10:
            a = "Noviembre";
            break;
        case 11:
            a = "Diciembre";
    }
    return a;
}
function setMesActual() {
    $("#nombre_mes").text(mesPorNumero(fecha_actual.mes + contador_mes) + " del " + fecha_actual.ano);
}
function primerDia(d1) {
    let primerdia;
    return (d2 = new Date(d1.getFullYear(), d1.getMonth() + contador_mes, 0)), d2.getDay();
}
function setFechasCalendario(d1, primer_dia, dias_mes) {
    var tds = $("#fechasMesCalendario td");
    let contador_de_dias = 1;
    for (i = 0; i < tds.length; i++) 0 !== primer_dia ? ((tds[i].innerHTML = ""), (primer_dia -= 1)) : 0 === primer_dia && contador_de_dias <= dias_mes && ((tds[i].innerHTML = contador_de_dias), contador_de_dias++);
}
function crearCalendario() {
    Date.prototype.monthDays = function () {
        let d;
        return new Date(this.getFullYear(), this.getMonth() + contador_mes + 1, 0).getDate();
    };
    let d1 = new Date();
    fecha_actual = { dia: d1.getDate(), mes: d1.getMonth(), ano: d1.getFullYear() };
    let dias_mes = d1.monthDays(),
        primer_dia = primerDia(d1);
    $("#fechasMesCalendario").find("td").text(""), setMesActual(), setFechasCalendario(d1, primer_dia, dias_mes);
}
function cargarContenidoAgenda(modulo) {
    let { contenido: contenido } = modulo.data;
    (dataCalendario = contenido.b), crearCalendario(), $("#mod_3_agenda").removeClass("hide");
}
function ponerContenidoModuloPorTipo({ modulo: modulo }) {
    switch (modulo.data.tipo) {
        case "0/Video":
            cargarContenidoVideos(modulo);
            break;
        case "1/Promociones":
            cargarContenidoPromociones(modulo);
            break;
        case "2/Turísmo":
            cargarContenidoTurismo(modulo);
            break;
        case "3/Agenda":
            cargarContenidoAgenda(modulo);
    }
}
$("body").on("click", ".btn_arrrow_calendario", function () {
    $("#fechasMesCalendario").find("td").removeClass("active");
    let orden = $(this).attr("data-mes");
    "after" === orden ? after_mes > 0 && ((contador_mes += 1), (after_mes -= 1), (before_mes += 1), crearCalendario()) : "before" === orden && before_mes > 0 && ((contador_mes -= 1), (before_mes -= 1), (after_mes += 1), crearCalendario());
}),
    $("#fechasMesCalendario")
        .find("td")
        .click(function () {
            $("#fechasMesCalendario").find("td").removeClass("active"), $(this).addClass("active"), $("#lista_eventos").empty();
            let dia = $(this).text();
            console.log(dia),
                console.log(fecha_actual.mes + contador_mes),
                dia.length > 0 &&
                    null !== dataCalendario &&
                    (console.log(dataCalendario),
                    $("#lista_eventos").append('<p class="titulo">Lista de eventos</p>'),
                    dataCalendario.forEach((e) => {
                        let f_dia = parseInt(e.fecha.split("-")[2]),
                            f_mes = parseInt(e.fecha.split("-")[1]),
                            f_ano = parseInt(e.fecha.split("-")[0]);
                        if (fecha_actual.ano === f_ano && fecha_actual.mes + contador_mes + 1 === f_mes && parseInt(dia) === f_dia) {
                            console.log("Entre"), console.log(e);
                            let html = '<div class="row evento">';
                            (html += '   <div class="col s12">'),
                                (html += "       <p>"),
                                (html += '           <span class="eti">Etiqueta: </span>'),
                                (html += '           <span class="texto">' + e.etiqueta + "</span>"),
                                (html += "       </p>"),
                                (html += "       <p>"),
                                (html += '           <span class="eti">Fecha: </span>'),
                                (html += '           <span class="texto">' + e.fecha + "</span>"),
                                (html += "       </p>"),
                                (html += "       <p>"),
                                (html += '           <span class="eti">Hora: </span>'),
                                (html += '           <span class="texto">' + e.hora + "</span>"),
                                (html += "       </p>"),
                                (html += "       <p>"),
                                (html += '           <span class="eti">Descripción: </span>'),
                                (html += '           <span class="texto">' + e.descripcion + "</span>"),
                                (html += "       </p>"),
                                (html += "   </div>"),
                                (html += "</div>"),
                                $("#lista_eventos").append(html);
                        }
                    }));
        }),
    $(".btn_icono").click(function () {
        let d = { button: $(this).attr("data-target"), maqueta: $(this).attr("data-maqueta"), tipo: $(this).attr("data-tipo"), cliente: $(this).attr("data-cliente"), titulo: $(this).attr("data-titulo") };
        cambiarTextoNotificacion("Obteniendo información"),
            mostrarLoader(!0),
            mostrarNotificacion(!0),
            console.log(d),
            "4/Puntos Wifi" !== d.tipo && "5/Visitas 360º" !== d.tipo
                ? peticionAjaxServidor(d, "POST", "json", url_server + "/newspoth/getDataModulo", (data) => {
                      0 === data.code
                          ? -1 !== data.modulo.id
                              ? ($(".mod_lista").addClass("hide"), ponerContenidoModuloPorTipo(data), $("#mod_titulo_span").text(d.titulo), $("#div_modulo").addClass("show"), mostrarLoader(!1), mostrarNotificacion(!1))
                              : (cambiarTextoNotificacion("Error al obtener la información"),
                                mostrarLoader(!1),
                                setTimeout(() => {
                                    mostrarNotificacion(!1);
                                }, 2500))
                          : (cambiarTextoNotificacion(data.data),
                            mostrarLoader(!1),
                            setTimeout(() => {
                                mostrarNotificacion(!1);
                            }, 2500));
                  })
                : "5/Visitas 360º" === d.tipo
                ? (cambiarTextoNotificacion("Proximamente, espera novedades"),
                  mostrarLoader(!1),
                  setTimeout(() => {
                      mostrarNotificacion(!1);
                  }, 2500))
                : "4/Puntos Wifi" === d.tipo &&
                  peticionAjaxServidor(d, "POST", "json", url_server + "/newspoth/getDataPuntosWifi", (data) => {
                      0 === data.code
                          ? data.equipos.length > 0
                              ? ($(".mod_lista").addClass("hide"), cargarContenidoPuntosWifi(data), $("#mod_titulo_span").text(d.titulo), $("#div_modulo").addClass("show"), mostrarLoader(!1), mostrarNotificacion(!1))
                              : (cambiarTextoNotificacion("Aun no hay puntos disponibles"),
                                mostrarLoader(!1),
                                setTimeout(() => {
                                    mostrarNotificacion(!1);
                                }, 2500))
                          : (cambiarTextoNotificacion(data.data),
                            mostrarLoader(!1),
                            setTimeout(() => {
                                mostrarNotificacion(!1);
                            }, 2500));
                  });
    }),
    $("#btn_login_trial").click(function () {
        if ((cambiarTextoNotificacion("Espere por favor..."), mostrarLoader(!0), mostrarNotificacion(!0), "none" !== user_name)) {
            let d;
            peticionAjaxServidor({ name: user_name, email: user_email, mac: macesc }, "POST", "json", url_server + "/newspoth/addUserEmailLikeButton", (data) => {
                0 === data.code
                    ? (location.href = linkloginonly + "?dst=" + linkorigesc + "&username=T-" + macesc)
                    : (cambiarTextoNotificacion("Error, recarga la página"),
                      mostrarLoader(!1),
                      setTimeout(() => {
                          mostrarNotificacion(!1);
                      }, 3e3));
            });
        }
    }),
    $("#btn_more_video").click(function () {
        let bloque = $(this).attr("data-bloque");
        $("#" + bloque).hasClass("show_texto")
            ? ($(this).text("keyboard_arrow_down"), $("#" + bloque).removeClass("show_texto"), $("#video").removeClass("show_texto"))
            : ($(this).text("keyboard_arrow_up"), $("#" + bloque).addClass("show_texto"), $("#video").addClass("show_texto"));
    }),
    $("#btn_more_imagen").click(function () {
        let bloque = $(this).attr("data-bloque");
        $("#" + bloque).hasClass("show_texto")
            ? ($(this).text("keyboard_arrow_down"), $("#" + bloque).removeClass("show_texto"), $("#imagen").removeClass("show_texto"))
            : ($(this).text("keyboard_arrow_up"), $("#" + bloque).addClass("show_texto"), $("#imagen").addClass("show_texto"));
    }),
    $("#cerrar_notificacion").click(function () {
        mostrarNotificacion(!1);
    });
