<?php
    session_start();
    require_once __DIR__ . '/src/Facebook/autoload.php';
    $fb = new Facebook\Facebook([
        'app_id' => '357269985151167',
        'app_secret' => 'cb3b8c5ae3a2ec818e2541a86401b437',
        'default_graph_version' => 'v3.3',
    ]);
      
    $helper = $fb->getRedirectLoginHelper();
      
    try {
        $accessToken = $helper->getAccessToken();
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
        // When Graph returns an error
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
        // When validation fails or other local issues
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }
      
    if (! isset($accessToken)) {
        if ($helper->getError()) {
          header('HTTP/1.0 401 Unauthorized');
          echo "Error: " . $helper->getError() . "\n";
          echo "Error Code: " . $helper->getErrorCode() . "\n";
          echo "Error Reason: " . $helper->getErrorReason() . "\n";
          echo "Error Description: " . $helper->getErrorDescription() . "\n";
        } else {
          header('HTTP/1.0 400 Bad Request');
          echo 'Bad request';
        }
        exit;
    }

    try {
        // Returns a `Facebook\FacebookResponse` object
        $response = $fb->get('/me?fields=id,name,email', ''. $accessToken .'');
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }
      
    $user = $response->getGraphUser();
      
    // Logged in
    $_SESSION['fb_access_token'] = (string) $accessToken;
    $_SESSION['user_name'] = $user['name'];
    $_SESSION['user_email'] = $user['email'];
    $_SESSION['login_status'] = 1;
    //header('Location: https://localhost:8443/spotweb');
    header('Location: https://prueba.newspoth.com');
?>