<?php
    require_once __DIR__ . '/src/Facebook/autoload.php';
    $login_fb_status = -1;
    $user_name = 'none';
    $user_email = 'none';

    //Poner negación cuando se publique if(!(isset($_POST['codigo']) && isset($_POST['macesc']))){
    if((isset($_POST['codigo']) && isset($_POST['macesc']))){
        session_start();
        //La petición es desde el callback de Facebook
        if(isset($_SESSION['fb_access_token']) && isset($_SESSION['user_name'])) {
            $login_fb_status = 1;
            $user_name = $_SESSION['user_name'];
            $user_email = $_SESSION['user_email'];
        } else {
            $login_fb_status = 0;
            $user_email = 'none';
            $user_name = 'none';
        }
    } else {
        //La petición es desde el equipo hotspot
        //Destruir variables de sesión que hayan en el navegador de cliente
        /* session_destroy();
        session_unset();
        session_start();

        $_SESSION['mac']=$_POST['mac'];
        $_SESSION['ip']=$_POST['ip'];
        $_SESSION['username']=$_POST['username'];
        $_SESSION['linklogin']=$_POST['link-login'];
        $_SESSION['linkorig']=$_POST['link-orig'];
        $_SESSION['error']=$_POST['error'];
        $_SESSION['trial']=$_POST['trial'];
        $_SESSION['chapid']=$_POST['chap-id'];
        $_SESSION['chapchallenge']=$_POST['chap-challenge'];
        $_SESSION['linkloginonly']=$_POST['linkloginonly'];
        $_SESSION['linkorigesc']=$_POST['linkorigesc'];
        $_SESSION['macesc']=$_POST['macesc'];
        $_SESSION['codigo']=$_POST['codigo']; */

        $_SESSION['codigo'] = 'Y3jzwRgNGejYthhMffXxrtUcBPCuqx';
        $_SESSION['mac'] = '00:00:00:00:00:00';
        $_SESSION['trial'] = true;
        $_SESSION['chapid'] = true;
        $_SESSION['error'] = '';
        $_SESSION['linkloginonly'] = '';
        $_SESSION['linkorigesc'] = '';
        $_SESSION['macesc'] = '';

        $login_fb_status = 0;
        $user_email = 'none';
        $user_name = 'none';

        //Solo para pruebas, borrar luego antes de publicar o comentar
        if(isset($_SESSION['fb_access_token']) && isset($_SESSION['user_name'])) {
            $login_fb_status = 1;
            $user_name = $_SESSION['user_name'];
            $user_email = $_SESSION['user_email'];
        }
    }
?>
<!doctype html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es">
<head>
    <title>Newspoth</title>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="apple-mobile-web-app-capable", content="yes">
    <meta name="author" content="André Montoya">
    <meta name="description" content="Aplicación para publicidad a través del hotspot">
    <meta name="keywords" content="publicidad, hotspot, promociona, vende, mejora negocio">

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="stylesheet" href="css/materialize.min.css">
    <link href='css/index.min.css' rel='stylesheet' type='text/css'>
    
    <script src='js/jquery-3.3.1.min.js'></script>
    <script src="js/md5.js"></script>
</head>
<body>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v4.0&appId=351056548987284&autoLogAppEvents=1"></script>
    <div class="landscape_div">
        <div class="contenido">
            <i class="material-icons">screen_rotation</i>
            <span>Gira tu dispositivo por favor, esta aplicación funciona en modo vertical (portrait) </span>
        </div>
    </div>
    <div class="anuncio" id="anuncio">
        <div class="cerrar_ad" id="cerrar_ad">
            <span id="contador" class="icono noactive" data-anuncio="" data-equipo="" data-cliente="" data-subcliente="">x</span>
        </div>
        <div class="video hide" id="video">
            <div class="ad_bloque">
                <div class="ad_titulo">
                    <div class="div_icono">
                        <span class="icono icono_anuncio">AD</span>
                    </div>
                    <div class="div_texto">
                        <span class="texto titulo_anuncio" id="ad_video_titulo">Titulo del anuncio</span>
                    </div>
                </div>
                <div class="ad_descripcion" id="ad_descripcion_video">
                    <span class="texto" id="ad_descripcion_video_text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore omnis hic vero quis laboriosam nulla provident esse eaque mollitia sunt asperiores voluptatem libero similique, iste quae, quidem nemo debitis magni?</span>
                    <div class="boton">
                        <span class="desc">Ver más</span>
                        <i class="material-icons flecha" id="btn_more_video" data-bloque="ad_descripcion_video">keyboard_arrow_down</i>
                    </div>
                </div>
                <div class="ad_file">
                    <video id="tag_video" controls autoplay muted>
                        <source id="src_video">
                    </video>
                </div>
            </div>
        </div>
        <div class="video hide" id="imagen">
            <div class="ad_bloque">
                <div class="ad_titulo">
                    <div class="div_icono">
                        <span class="icono icono_anuncio">AD</span>
                    </div>
                    <div class="div_texto">
                        <span class="texto titulo_anuncio" id="ad_imagen_titulo">Titulo del anuncio</span>
                    </div>
                </div>
                <div class="ad_descripcion" id="ad_descripcion_imagen">
                    <span class="texto" id="ad_descripcion_imagen_text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore omnis hic vero quis laboriosam nulla provident esse eaque mollitia sunt asperiores voluptatem libero similique, iste quae, quidem nemo debitis magni?</span>
                    <div class="boton">
                        <span class="desc">Ver más</span>
                        <i class="material-icons flecha" id="btn_more_imagen" data-bloque="ad_descripcion_imagen">keyboard_arrow_down</i>
                    </div>
                </div>
                <div class="ad_file">
                    <img src="" id="src_imagen">
                </div>
            </div>
        </div>
    </div>
    <div id="fondo" class="fondo">
        <div class="banner" id="banner"></div>
        <div class="imagen" id="img_fondo"></div>
        <div class="social" id="social">
            <?php if ($login_fb_status == 0) {?> 
                <div class="div_social login_facebook" id="nologinfb">
                    <div class="centro">
                        <span class="texto">Para continuar</span>
                        <?php
                            $fb = new Facebook\Facebook([
                                'app_id' => '357269985151167',
                                'app_secret' => 'cb3b8c5ae3a2ec818e2541a86401b437',
                                'default_graph_version' => 'v3.3',
                            ]);

                            $helper = $fb->getRedirectLoginHelper();

                            $permissions = ['email']; // Optional permissions
                            //$loginUrl = $helper->getLoginUrl('https://localhost:8443/spotweb/fb-callback.php', $permissions);
                            $loginUrl = $helper->getLoginUrl('https://prueba.newspoth.com/fb-callback.php', $permissions);

                            echo '<a id="login_fb" class="btn_acction" href="' . htmlspecialchars($loginUrl) . '">Danos  un Like en Facebook</a>';
                        ?>
                    </div>
                </div>
            <?php } else { ?>
                <div class="div_social" id="likefb">
                    <div class="centro">
                        <span class="texto">Regálanos un like</span>
                        <div class="fb-like" data-href="https://www.facebook.com/NettplusCiaLtda/" data-width="" data-layout="button_count" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div>
                        <span id="mensaje_temporal" class="texto hide">Espere por favor</span>
                    </div>
                </div>
                <div class="div_social login_internet hide" id="login_mac">
                    <div class="centro">
                        <?php if($_SESSION['chapid']) { ?>
                            <form id="login" name="sendin" action="<?php echo $_SESSION['linkloginonly']; ?>" method="post">
                                <input type="hidden" name="username" id="username"/>
                                <input type="hidden" name="password" id="password" />
                                <input type="hidden" name="dst" value="https://google.com" />
                                <input type="hidden" name="popup" value="true" />
                                <span class="texto">¿Eres cliente?</span>
                                <input type="submit" class="btn_acction" id="btn_login_mac" value="Ingresa aquí">
                            </form>

                            <script type="text/javascript">
                                $('#username').val('<?php echo $_SESSION['mac']; ?>');
                                $('#password').val(hexMD5('<?php echo $_SESSION['chapid']; ?><?php echo $_SESSION['mac']; ?><?php echo $_SESSION['chapchallenge']; ?>'));
                            </script>
                        <?php } ?>
                    </div>
                </div>
                <div class="div_social login_internet hide" id="login_trial">
                    <div class="centro">
                        <?php if($_SESSION['trial']) { ?>
                            <span class="texto">¿No eres cliente?</span>
                            <button id="btn_login_trial" class="btn_acction">Ingresa aquí</button>
                        <?php } else {?>
                            <span class="texto">Tiempo agotado</span>
                            <button class="btn_acction">Gracias</button>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?> 
        </div>
        <div class="botones" id="botones">
            <div class="btn_padre" id="btn_a" data-button="a">
                <div class="btn_icono" id="btn_a_icono" data-target="a"></div>
                <span class="btn_texto" id="btn_a_texto">T1</span>
            </div>
            <div class="btn_padre" id="btn_b" data-button="b">
                <div class="btn_icono" id="btn_b_icono" data-target="b"></div>
                <span class="btn_texto" id="btn_b_texto">T2</span>
            </div>
            <div class="btn_padre" id="btn_c" data-button="c">
                <div class="btn_icono" id="btn_c_icono" data-target="c"></div>
                <span class="btn_texto" id="btn_c_texto">T3</span>
            </div>
            <div class="btn_padre" id="btn_d" data-button="d">
                <div class="btn_icono" id="btn_d_icono" data-target="d"></div>
                <span class="btn_texto" id="btn_d_texto">T4</span>
            </div>
        </div>
    </div>
    <div class="notificacion show" id="notificacion">
        <div class="centro">
            <span class="icono">N</span>
            <span class="texto" id="noti_texto">Obteniendo información...</span> 
            <div class="preloader-wrapper small active loader hide" id="loader">
                <div class="spinner-layer spinner-white-only">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                    <div class="circle"></div>
                </div><div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
                </div>
            </div>
            <i class="material-icons hide icono_btn" id="cerrar_notificacion">close</i>
        </div>
    </div>
    <div class="div_modulo" id="div_modulo">
        <div class="mod_snac">
            <div class="mod_opciones">
                <i class="icono material-icons" id="cerrar_div_modulo" data-target="">keyboard_arrow_left</i>
            </div>
            <div class="mod_titulo">
                <span class="texto" id="mod_titulo_span">Titulo de prueba</span>
            </div>
        </div>
        <div class="mod_contenido">
            <!--Completado-->
            <div class="mod_lista hide" id="mod_0_video">
                <div class="lista_items all_height" id="lista_videos">
                    <div class="video">
                        <div class="video_titulo">
                            <i class="material-icons icono">video_library</i>
                            <span class="nombre">Nombre del video</span>
                        </div>
                        <div class="video_descripcion">
                            <span class="texto">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquam eius dolore repellat quod voluptates harum reiciendis cum esse natus odit perspiciatis inventore, veritatis officiis voluptas rerum. Ipsa fugiat illum eum. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolorum quo recusandae laboriosam beatae, expedita maxime. Quos, earum temporibus et expedita deleniti sunt dignissimos facilis, fuga aliquid tempora molestiae, sapiente ullam.</span>
                            <div class="opcion">
                                <span>Ver más</span>
                                <i class="material-icons icono">keyboard_arrow_down</i>
                            </div>
                        </div>
                        <div class="video_media">
                            <video controls>
                                <source id="source_video" src="">
                            </video>
                            <div class="miniatura">
                                <i class="material-icons">play_arrow</i>
                                <div class="preloader-wrapper small active loader">
                                    <div class="spinner-layer spinner-red-only">
                                    <div class="circle-clipper left">
                                        <div class="circle"></div>
                                    </div><div class="gap-patch">
                                        <div class="circle"></div>
                                    </div><div class="circle-clipper right">
                                        <div class="circle"></div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Completado-->
            <div class="mod_lista hide" id="mod_1_promociones">
                <div class="lista_items all_height" id="lista_promociones">
                </div>
            </div>
            <!--Completado-->
            <div class="mod_lista hide mod_turismo" id="mod_2_turismo">
                <div class="lista_cat" id="lista_cat">
                </div>
                <div class="lista_items" id="lista_lugares">
                </div>
            </div>
            <!--Completado, funciona por año actual-->
            <div class="mod_lista mod_lista_agenda hide" id="mod_3_agenda">
                <div class="contenedor">
                    <div class="meses">
                        <i class="material-icons icono izq btn_arrrow_calendario" data-mes="before">keyboard_arrow_left</i>
                        <span class="nombre_mes" id="nombre_mes">Nombre Mes</span>
                        <i class="material-icons icono der btn_arrrow_calendario" data-mes="after">keyboard_arrow_right</i>
                    </div>
                    <div class="tabla">
                        <table>
                            <thead>
                                <tr>
                                    <td>Lun</td><td>Mar</td><td>Mie</td><td>Jue</td><td>Vie</td><td>Sab</td><td>Dom</td>
                                </tr>
                            </thead>
                            <tbody id="fechasMesCalendario">
                                <tr>
                                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                </tr>
                                <tr>
                                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                </tr>
                                <tr>
                                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                </tr>
                                <tr>
                                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                </tr>
                                <tr>
                                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                </tr>
                                <tr class="removible">
                                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                </tr>
                                <tr class="removible">
                                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="lista_eventos" id="lista_eventos">
                            
                        </div>
                    </div>
                </div>
            </div>
            <!--Completado-->
            <div class="mod_lista hide mod_puntos_wifi" id="mod_4_puntos_wifi">
                <div class="lista_cat" id="lista_cantones">
                </div>
                <div class="lista_items" id="lista_puntos">
                </div>
            </div>
            <!--Pendiente-->
            <div class="mod_lista hide" id="mod_5_visitas_360">
                
            </div>
        </div>
    </div>
    <script src='js/materialize.min.js'></script>
    <script src='js/index.js'></script>
    <script>
        const linkloginonly = '<?php echo $_SESSION['linkloginonly']; ?>';
        const linkorigesc = '<?php echo $_SESSION['linkorigesc']; ?>';
        const macesc = '<?php echo $_SESSION['macesc']; ?>';
        const codigo = '<?php echo $_SESSION['codigo'];?>';
        const mac_cliente = '<?php echo $_SESSION['mac'];?>';
            
        const login_fb_status = '<?php echo $login_fb_status; ?>';
        const user_name = '<?php echo $user_name; ?>';
        const user_email = '<?php echo $user_email; ?>';
        const url_server = 'https://admin.newspoth.com';

        //Make Request to Server
        start(codigo, mac_cliente, url_server);

    </script>
</body>
</html>
